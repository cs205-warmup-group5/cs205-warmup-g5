# CS 205 Warmup Project Group 5

## Instructions
To test our project, all you need to do is clone the repo. Only ever use run.py and run the program in some sort of terminal for 
optimal use. Once you use the command 'load data', you are free to test any query you please. The parsing logic is very hardcoded, 
so only queries formatted as specified will work. Use the 'help' command to see a list of queries, their formats, and what the return.