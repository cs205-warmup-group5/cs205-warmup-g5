'''

    CS 205 Warmup Project - Queries

'''

import db, time, os

data_loaded = False



def main():

    global data_loaded

    # determines if data has been loaded or not
    try:
        with open("dl.txt") as file:
            status = file.readline()
            if status.rstrip() == "F":
                data_loaded = False
            else:
                data_loaded = True
    except:
        data_loaded = False

    query = ""
    while (query.lower() != "exit"):

        cls()

        print("--------------------------------")
        print()
        print("Welcome to the olympics command line.")
        print("If a term such as a country or athlete is two or more words, enclose the")
        print("term in single quotes, as such: Average Age 'North Korea'")
        print("Type 'load data' before making any queries.")
        print("Type 'help' for more info.")
        print()
        print("Enter your query below, or type \"exit\" to leave.")
        
        query = input("Your query: ")
        print()

        if query.lower() != "exit":

            # parse query
            data = parse(query)

            # print query result
            print(data)

            # time out to let user view query result
            if data == "help":
                time.sleep(10)
            else:
                time.sleep(5)

        else:
            
            print()
            print("Exiting...")
            time.sleep(2)
            cls()

def parse(query):

    global data_loaded

    '''
    
        Parse query to pass to db.

        Parameters:
            query (str) : query to be parsed
        Returns: 
            data (str) : data returned by queries
    
    '''

    # separate query into an array of words
    query_terms = split_terms(query)

    # import array of different categories
    events = []
    countries = []
    medals = ["Gold", "Silver", "Bronze"]
    names = []

    # filler
    data = "Bad query."

    # determines query type and makes function calls
    try:
        if (query.lower() == "load data" and not data_loaded):
            if (db.load_data()):
                data = "Data sucessfully loaded."
                with open("dl.txt", "w") as file:
                    file.write("T")
                data_loaded = True
            else:
                data = "ERROR: Error loading data."
        elif (data_loaded):
            if (len(query_terms) == 3):
                events = db.make_event_array()
                countries = db.make_country_array()
                names = db.make_name_array()
                # determine which of the three types of 3 term queries it is
                if query_terms[0].lower() == "average" and query_terms[2] in countries:
                    data = db.average_age(query_terms[2])
                elif query_terms[0].lower() == "total" and query_terms[1].lower() == "medals" and query_terms[2] in countries:
                    data = db.query_total_medals(query_terms[2])
                elif query_terms[0].lower() == "total" and query_terms[1].lower() == "medals" and query_terms[2] in names:
                    data = db.athlete_country_medals(query_terms[2])
            elif (len(query_terms) == 2):
                events = db.make_event_array()
                countries = db.make_country_array()
                names = db.make_name_array()
                # determine which of the three types of 2 term queries it is
                if query_terms[0].lower() == "age" and query_terms[1] in names:
                    data = db.athlete_age(query_terms[1])
                elif query_terms[0].lower() == "athletes" and query_terms[1] in events:
                    data = db.query_event_players(query_terms[1])
                elif query_terms[0] in medals and query_terms[1] in countries:
                    data = db.query_num_medals(query_terms[1], query_terms[0])
                elif (query.lower() == "load data"):
                    if (data_loaded):
                        data = "Data already loaded."
                elif (query.lower() == "row athletes"):
                    data = db.num_rows_athletes()
                elif (query.lower() == "row medals"):
                    data = db.num_rows_medals()
            elif (len(query_terms) == 1):
                if (query.lower() == "help"):
                    data = "help"
                    help()
            else:
                pass
        else:
            data = "ERROR: Data not yet loaded."

    except Exception as e:

        data = "Bad Query."

    # convert data from array to string
    data_string = ""

    if (data != "Bad Query." and type(data) != str):
        if (len(data) == 0):
            data_string = "Bad Query."
        elif (len(data) == 1):
            data_string = data[0]
        else:
            for index in range(len(data)):
                data_string += data[index]
                if (index != len(data) - 1):
                    data_string += ", "
                if (index % 10 == 0 and index != 0):
                    data_string += "\n"
        data = data_string


    return data

def split_terms(query):

    '''
    
        Splits terms of query

        Parameters:
            query (str) : query to be split
        Returns: 
            query_terms (list) : query terms split
    
    '''

    query_terms = []

    if ("'" in query):
        
        # splits terms at the quotation
        first_split = query.split("'")
        first_split.pop(len(first_split) - 1)

        # splits terms at the space and combines them back together
        second_split = first_split[0].split()
        second_split.append(first_split[len(first_split) - 1])

        query_terms = second_split

    else:
        query_terms = query.split()

    return query_terms

def print_lines(output):

    '''
    
        Prints lines to clean up output.
    
    '''

    print()

    for i in range(output):
        print("--------------------------------")

    print()


def cls():

    '''

        Clears console.

    '''

    os.system("cls" if os.name=="nt" else "clear")

def help():

    '''
    
        Prints queries for user to enter.
    
    '''

    print("Example queries: ")
    print("*** Make sure to capitalize all nouns.")
    print("")
    print("Average Age 'United States")
    print("Returns average age of U.S. athletes.")
    print("")
    print("Total Medals 'Great Britian'")
    print("Total medals won by G.B.")
    print("")
    print("Age 'John Doe'")
    print("Age of inputted athlete.")
    print("")
    print("Total medals 'John Doe'")
    print("Total medals won by inputted athlete.")
    print("")
    print("Gold USA")
    print("Gold/Silver/Bronze total medals won by inputted country")
    print("*** Make sure medal name is capitalized")
    print("")
    print("Athletes 'Synchronized Swimming'")
    print("All athletes in specified event")
    print("")
    print("Row Medals")
    print("Number of rows in medals table")
    print("")
    print("Row Athletes")
    print("Number of rows in athletes table")
    print("")
    print("Load Data")
    print("Loads data into table.")

