'''

    all the code for collecting and returning the queried data can go in here

'''
from _warnings import filters
from symbol import parameters

import query
import sqlite3
from sqlite3 import Error
import csv


def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connected")
    except Error as e:
        print(f"Error '{e}' occurred")

    return connection


conn = create_connection("sqlite\\db\\database.sqlite")


def create_table_athletes(connection, sql):
    try:
        cursor = connection.cursor()
        cursor.execute(sql)
        athlete_file = open("athlete_events_cleaned.csv")
        athlete_contents = csv.reader(athlete_file)
        insert_athletes = "INSERT OR IGNORE INTO Athletes (Country, Name, Age, Sport) VALUES(?, ?, ?, ?);"
        cursor.executemany(insert_athletes, athlete_contents)
        connection.commit()
    except Error as e:
        print(f"Error '{e} occurred")


def create_table_medals(connection, sql):
    try:
        cursor = connection.cursor()
        cursor.execute(sql)
        medal_file = open("Summer_olympic_Medals_cleaned.csv")
        medal_contents = csv.reader(medal_file)
        insert_medals = "INSERT OR IGNORE INTO Medals(Country, Gold, Silver, Bronze) VALUES(?,?,?,?)"
        cursor.executemany(insert_medals, medal_contents)
        connection.commit()
    except Error as e:
        print(f"Error '{e} occurred")


def average_age(country):
    cursor = conn.cursor()
    sql = """SELECT AVG(Age) FROM Athletes WHERE Country = ?"""
    cursor.execute(sql, (country,))
    age = cursor.fetchall()
    age = [item[0] for item in age]
    return age


def query_num_medals(country, medal):
    cursor = conn.cursor()
    if medal == "Gold":
        sql = """SELECT Gold FROM Medals WHERE Country = ?"""
        cursor.execute(sql, (country,))
    if medal == "Silver":
        sql = """SELECT Silver FROM Medals WHERE Country = ?"""
        cursor.execute(sql, (country,))
    if medal == "Bronze":
        sql = """SELECT Bronze FROM Medals WHERE Country = ?"""
        cursor.execute(sql, (country,))
    total = cursor.fetchall()
    total = [item[0] for item in total]
    return total


def query_total_medals(country):
    cursor = conn.cursor()
    sql = """SELECT Gold + Silver + Bronze as MedalTotal FROM Medals WHERE Country = ?"""
    cursor.execute(sql, (country,))
    total = cursor.fetchall()
    total = [item[0] for item in total]
    return total


def athlete_age(name):
    sql = """SELECT Age FROM Athletes WHERE Name = ?"""
    cursor = conn.cursor()
    cursor.execute(sql, (name,))
    age = cursor.fetchall()
    age = [item[0] for item in age]
    return age


def athlete_country_medals(name):
    sql = """SELECT Gold + Silver + Bronze as MedalTotal FROM Medals INNER JOIN Athletes ON Medals.Country = Athletes.Country WHERE Athletes.Name = ?"""
    cursor = conn.cursor()
    cursor.execute(sql, (name,))
    medals = cursor.fetchall()
    medals = [item[0] for item in medals]
    return medals


def query_event_players(event):
    sql = """SELECT Name FROM Athletes WHERE Sport = ?"""
    cursor = conn.cursor()
    cursor.execute(sql, (event,))
    names = cursor.fetchall()
    names = [item[0] for item in names]
    return names


def make_country_array():
    cursor = conn.cursor()
    sql = """SELECT Country FROM Medals"""
    cursor.execute(sql)
    countries = cursor.fetchall()
    country_array = [item[0] for item in countries]
    return country_array


def make_event_array():

    global conn
    cursor = conn.cursor()
    event_sql = """SELECT Sport FROM Athletes"""
    cursor.execute(event_sql)
    events = cursor.fetchall()
    events_array = [item[0] for item in events]
    return events_array


def make_name_array():
    cursor = conn.cursor()
    name_sql = """SELECT Name FROM Athletes"""
    cursor.execute(name_sql)
    names = cursor.fetchall()
    name_array = [item[0] for item in names]
    return name_array


def make_age_array():
    cursor = conn.cursor()
    age_sql = """SELECT Age FROM Athletes"""
    cursor.execute(age_sql)
    age = cursor.fetchall()
    age_array = [item[0] for item in age]
    return age_array


def load_data():

    global conn

    try:
        athlete_table_sql = """CREATE TABLE IF NOT EXISTS Athletes(
                                Name text PRIMARY KEY,
                                Age integer NOT NULL,
                                Sport text NOT NULL,
                                Country text,
                                FOREIGN KEY (Country) REFERENCES Medals(Country)
                                )"""

        medal_table_sql = """CREATE TABLE IF NOT EXISTS Medals(
                                Country text PRIMARY KEY,
                                Gold integer NOT NULL,
                                Silver integer NOT NULL,
                                Bronze integer NOT NULL
                                )"""

        conn = create_connection("sqlite\\db\\database.sqlite")

        create_table_athletes(conn, athlete_table_sql)
        create_table_medals(conn, medal_table_sql)

        cursor = conn.cursor()
        sql = """SELECT Country FROM Medals"""
        cursor.execute(sql)
        countries = cursor.fetchall()
        country_array = [item[0] for item in countries]
        # print(country_array)

        event_sql = """SELECT Sport FROM Athletes"""
        cursor.execute(event_sql)
        events = cursor.fetchall()
        events_array = [item[0] for item in events]
        # print(events_array)

        name_sql = """SELECT Name FROM Athletes"""
        cursor.execute(name_sql)
        names = cursor.fetchall()
        name_array = [item[0] for item in names]
        # print(name_array)

        age_sql = """SELECT Age FROM Athletes"""
        cursor.execute(age_sql)
        age = cursor.fetchall()
        age_array = [item[0] for item in age]
        # print(age_array)

        return True

    except:
        return False

def num_rows_medals():
    cursor = conn.cursor()
    row_sql = """SELECT COUNT(Country) FROM Medals"""
    cursor.execute(row_sql)
    medal_rows = cursor.fetchall()
    medal_row_array = [item[0] for item in medal_rows]
    return medal_row_array


def num_rows_athletes():
    cursor = conn.cursor()
    row_sql = """SELECT COUNT(Country) FROM Athletes"""
    cursor.execute(row_sql)
    athlete_rows = cursor.fetchall()
    athlete_row_array = [item[0] for item in athlete_rows]
    return athlete_row_array


    


def main():
    # If user query is load data call load_data function
    load_data()

    # If user query is help call help function
    print(query_num_medals("Denmark", "Gold"))
    print(query_num_medals("Russia", "Silver"))
    print(query_num_medals("Denmark", "Bronze"))
    print(athlete_age("Wang Yihan"))
    print(query_total_medals("Denmark"))
    print(average_age("Denmark"))
    print(athlete_country_medals("Taylor Phinney"))
    print(athlete_country_medals("Magorzata Jasiska"))
    print(query_event_players("Cycling"))


if __name__ == '__main__':
    main()